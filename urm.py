#!/usr/bin/env python

import urwid as ui

from sys import exit
from os import environ
from datetime import timedelta
from ampcontrol import ampcontrol, parse_host


if __name__ == '__main__':


    # setting up the client object
    if environ.has_key('MPD_HOST'):
        serverstring = environ['MPD_HOST']
        serverstring = serverstring+':'+environ['MPD_PORT'] if environ.get('MPD_PORT') else serverstring

    else:
        serverstring = 'localhost'


    server = parse_host(serverstring)
    client = ampcontrol()

    client.connect(server['host'], server['port'])
    if server['password']:  
        client.password(server['password'])

    
    # setting up the urwid display

    palette = [
        ('time', 'black', 'dark green'),
        ('artist', 'black', 'dark cyan'),
        ('title', 'black', 'brown'),
        ('album', 'black', 'dark magenta'),
    ]

    playlist = client.playlistinfo()

    songs = []
    for song in playlist:

        #song = playlist[i]

        time = str(timedelta(seconds=int(song['time']))).lstrip(':0')
        artist = song['artist'] if song.has_key('artist') else ''
        title = song['title'] if song.has_key('title') else ''
        album = song['album'] if song.has_key('album') else ''

        columns = [
            ui.AttrMap(ui.Text(time, wrap='clip'), 'time'),
            ui.AttrMap(ui.Text(artist, wrap='clip'), 'artist'),
            ui.AttrMap(ui.Text(title, wrap='clip'), 'title'),
            ui.AttrMap(ui.Text(album, wrap='clip'), 'album')
        ]

        songs.append(ui.Columns(columns, dividechars=1))

    pl_walker = ui.SimpleListWalker(songs)
    pl = ui.ListBox(pl_walker)

    loop = ui.MainLoop(pl, palette)
    loop.run()
