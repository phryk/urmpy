#!/usr/bin/env python

"""
Usage:
  ampcontrol [options] play
  ampcontrol [options] stop
  ampcontrol [options] pause
  ampcontrol [options] prev
  ampcontrol [options] next
  ampcontrol [options] fade <start> <end> [<duration>]
  ampcontrol [options] add <directory> [<hierarchy>]

Options:
  -h --help             Show this screen.
  -s --server <server>  Specify the server ([<password>@]<host>[:<port>] notation), the $MPD_HOST and $MPD_PORT environment variables are used as fallback.
"""

from sys import exit
from os import environ
from time import sleep
from mpd import MPDClient
from docopt import docopt

class ampcontrol(MPDClient):

    def fade(self, start, end, duration = 1000):

        if not duration:
            duration = '1000'

        if not duration.isdigit():
            raise TypeError('duration must be integerish.')

        duration = int(duration)

        status = self.status()


        if start == 'current':
            start = status['volume']

        elif end == 'current':
            end = status['volume']

        if not (start.isdigit() and end.isdigit()):
            raise TypeError('start and end volumes have to be integerish.') #start or end volume is neither 'current' nor integer

        #cast args into integers
        start = int(start)
        end = int(end)

        if start == end:
            exit(0) #do nothing and exit

       
        stepd = 100 #step length, in ms
        delta = end - start
        steps = int(round(duration / stepd))
        if steps == 0:
            self.setvol(end)
            exit(0)

        step = float(delta) / float(steps) #only linear fade for now


        self.setvol(start)
        # end volume over 0 means playing
        if end > 0 and status['state'] != 'playing':
            self.play()

        for i in range(steps):
            nvol = int(round(int(start) + (step * ( i + 1 ) ) ))
            self.setvol(nvol)

            if i == (steps -1):
                self.setvol(end)
            else:
                sleep(abs(stepd / 1000.0))


        if(end == 0):
            self.stop()



def parse_host(s):

    server = {
        'password': None,
        'host': None,
        'port': None,
    }

    if s.find('@') > 0:
      server['password'] = s[0:s.find('@')]
      s = s[s.find('@') + 1:]


    if s.find(':') > 0:
      server['host'] = s[0:s.find(':')]
      server['port'] = s[s.find(':') + 1:]

    else:
      server['host'] = s
      server['port'] = 6600

    return server


if __name__ == '__main__':
    args = docopt(__doc__)

    if(args['--server']):
        serverstring = args['--server']

    elif(environ.has_key('MPD_HOST')):
        serverstring = environ['MPD_HOST']
        serverstring = serverstring+':'+environ['MPD_PORT'] if environ.get('MPD_PORT') else serverstring

    else:
        serverstring = 'localhost:6600'

    server = parse_host(serverstring)

    client = ampcontrol()
    client.connect(server['host'], server['port'])

    if server.has_key('password'):  
        client.password(server['password'])


    if args['play']:
        client.play()

    elif args['stop']:
        client.stop()

    elif args['pause']:
        client.pause()

    elif args['prev']:
        client.previous()

    elif args['next']:
        client.next()

    elif args['fade']:
        start = args['<start>']
        end = args['<end>']
        duration = args['<duration>']

        client.fade(start, end, duration)
